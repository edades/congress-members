import React from 'react'
import axios from 'axios'
import { connect } from 'react-redux';
import { setMembers, setFilteredMembers } from '../../store/actions'
import Member from '../../components/Member'
import SearchBar from '../../components/SearchBar'


class Home extends React.Component {
  state = {
    hasSearch: false
  }

  componentDidMount() {
    this.getData()
  }

  handlerFilter = (value) => {
    this.setState({ hasSearch: value.length })
    const filteredData = this.props.members.filter(o =>
      Object.keys(o).some(k => o[k].toLowerCase().includes(value.toLowerCase())));
      this.props.setFilteredMembers(filteredData)
  }

  getData = () => {
    axios.get('https://api.propublica.org/congress/v1/115/senate/members.json', {
      headers: {
        'X-API-Key': process.env.REACT_APP_API_KEY
      }
    }).then(response => {
      const members = response.data.results[0].members
      const normalizedMembers = members.map(member => ({
        id: member.id,
        birth: member.date_of_birth || '',
        firstName: member.first_name || '',
        lastName: member.last_name || '',
        party: member.party || '',
        phone: member.phone || '',
        gender: member.gender || ''
      }))
      this.props.setMembers(normalizedMembers)
    }).catch(err => console.error('ha ocurrido un error: ', err))
  }

  render(){
    if (!this.props.members.length) {
      return <p>Loading data...</p>
    }

    const membersData = this.state.hasSearch ? this.props.filteredMembers : this.props.members;
    return (
      <div className="home">
        <div className="content">
          <div className="contentHeader">
            <h1>List of Members </h1>
            <SearchBar filterData={this.handlerFilter} />
          </div>          
          <ul className="contentMembers">
            {membersData.length
              ? membersData.map(member => <Member member={member} key={member.id} />)
              : <p>No results found</p>}
          </ul>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ membersReducer }) => ({
  members: membersReducer.members,
  filteredMembers: membersReducer.filteredMembers
});
const mapDispatchToProps = dispatch => {
  return {
    setMembers : (data) => dispatch(setMembers(data)),
    setFilteredMembers: (data) => dispatch(setFilteredMembers(data))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home)