import React, { useState, useEffect } from 'react';
import axios from 'axios'
import { useParams, Link } from "react-router-dom";

const Detail = () => {
  const { id } = useParams();
  const [ member, setMember ] = useState(null)
  
  useEffect(() => {
    getData()
  }, [])

  const getData = () => {
    axios.get(`https://api.propublica.org/congress/v1/members/${id}.json`, {
      headers: {
        'X-API-Key': process.env.REACT_APP_API_KEY
      }
    }).then(response => {
      const tempMember = response.data.results.length 
        ? response.data.results[0]
        : { firstName: 'not_found' }
      const normalizedMember = {
        firstName: tempMember.first_name,
        lastName: tempMember.last_name,
        gender: tempMember.gender,
        url: tempMember.url
      }
      setMember(normalizedMember)
    }).catch(err => console.error('Error: ', err))
  }

  if (!member) {
    return <p>Loading member info...</p>
  }

  if (member && member.firstName === 'not_found') {
    return <p>Member don't exist <Link to="/">Back to home</Link></p>
  }

  return (
    <div className="detail">
      <p>{member.firstName} {member.lastName}</p>
      <p>Gender: {member.gender}</p>
      <a href={member.url} target="_blank" rel="noopener noreferrer">Member URL</a>
      <br />
      <Link to="/">Back to home</Link>
    </div>
  )
}

export default Detail