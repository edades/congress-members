const setMembers = (data) => {
  return {
    type: 'SET_MEMBERS',
    payload: data
  }
}

const setFilteredMembers = (data) => {
  return {
    type: 'SET_FILTERED_MEMBERS',
    payload: data
  }
}

export {
  setMembers,
  setFilteredMembers
}