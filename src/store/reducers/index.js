import { combineReducers } from 'redux';

const membersInitialState = {
  members: [],
  filteredMembers: []
}

const membersReducer = (initialState = membersInitialState, action) => {
  switch (action.type) {
    case 'SET_MEMBERS':
      return Object.assign({}, initialState, {
        members: action.payload
      })
    case 'SET_FILTERED_MEMBERS':
      return Object.assign({}, initialState, {
        filteredMembers: action.payload
      })
    default:
      return initialState
  }
}

export default combineReducers({
  membersReducer
});