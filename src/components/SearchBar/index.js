import React from 'react';
import _ from 'lodash'

class SearchBar extends React.Component {
  state = {
    param: ''
  }

  handleSearchDebounced = _.debounce(() =>
    this.props.filterData(this.state.param), 500);

  onChange = (e) => {
    const { value } = e.target
    this.setState({
      param: value
    })
    this.handleSearchDebounced()
  }

  render() {
    return(
      <input type="text" placeholder="Search by any term..." value={this.state.param} onChange={this.onChange} />
    )
  }
}

export default SearchBar