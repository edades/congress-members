import React from 'react'
import { Link } from "react-router-dom";

const Member = ({ member }) => {
  const {
    id,
    firstName,
    lastName,
    birth,
    phone
  } = member
  return (
    <li className="member">
      <Link to={`/${id}`}>
        <div className="memberHeader">{firstName} {lastName}</div>
        <div className="memberInfo">
          <p><strong>Birth:</strong> {birth}</p>
          <p><strong>Phone:</strong> {phone}</p>
        </div>
      </Link>
    </li>
  )
}

export default Member