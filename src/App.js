import React from 'react';
import Home from './containers/Home';
import Detail from './containers/Detail'
import Header from './components/Header'
import Footer from './components/Footer'

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/:id">
            <Detail />
          </Route>
        </Switch>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
